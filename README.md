# e3-pvxs

This is an e3 wrapper for PVXS EPICS base module testing. PVXS is a new C++ implementation of the pvDataCPP and pvAccessCPP. 

---

## Wrapper template

This README.md should be updated as part of creation and should add complementary information about the wrapped module in question (usage, etc.). Once the repository is set up, empty/unused directories should also be purged.
