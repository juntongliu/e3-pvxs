#
#  Copyright (c) 2019 - 2021, European Spallation Source ERIC
#
#  The program is free software: you can redistribute it and/or modify it
#  under the terms of the BSD 3-Clause license.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.
# 
# Author  : Juntong Liu
# email   : juntong.liu@ess.eu
# Date    : 2021-09-16
# version : 0.0.0 
#
# This template file is based on one generated by e3TemplateGenerator.bash.
# Please look at many other Makefile.E3 in the https://gitlab.esss.lu.se/epics-modules/ 
# repositories.
# 

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS

TOP=..

# JT, add cpp flags for pvxs
USR_CPPFLAGS += -DPVXS_API_BUILDING
USR_CPPFLAGS += -DPVXS_ENABLE_EXPERT_API

#ifeq (,$(PVXS_MAJOR_VERSION))
#$(error PVXS_MAJOR_VERSION undefined, problem reading cfg/CONFIG_PVXS_VERSION)
#endif

############################################################################
#
# Add any required modules here that come from startup scripts, etc.
#
############################################################################

# REQUIRED += stream

#####################
# Dependent modules
# ###################


############################################################################
#
# If you want to exclude any architectures:
#
############################################################################

# EXCLUDE_ARCHS += linux-ppc64e6500


############################################################################
#
# If you want to allow specific architectures only:
#
############################################################################

# ARCH_FILTER += linux-x86_64


############################################################################
#
# Relevant directories to point to files
#
############################################################################

#APP:=pvxs
APP:=src
IOC:=ioc
#APPDB:=$(APP)/Db
#APPSRC:=$(APP)/src
APPSRC:=$(APP)
IOCSRC:=$(IOC)
#APPCMDS:=$(APP)/cmds
#APPINCLUDE:=$(APP)/src/pvxs
APPINCLUDE:=$(APP)/pvxs

############################################################################
#
# Add any files that should be copied to $(module)/db
#
############################################################################

# TEMPLATES += $(wildcard $(APPDB)/*.db)
# TEMPLATES += $(wildcard $(APPDB)/*.proto)
# TEMPLATES += $(wildcard $(APPDB)/*.template)

# USR_INCLUDES += -I$(where_am_I)$(APPSRC)
USR_INCLUDES += -I$(where_am_I)$(APPSRC)

#USR_INCLUDES += -I$(where_am_I)$(APPSRC)/pvxs

# JT, expand
# see below for special case versionNum.h
EXPAND += describe.h

EXPANDVARS += PVXS_MAJOR_VERSION
EXPANDVARS += PVXS_MINOR_VERSION
EXPANDVARS += PVXS_MAINTENANCE_VERSION
EXPANDVARS += EPICS_HOST_ARCH T_A OS_CLASS

EXPANDFLAGS += $(foreach var,$(EXPANDVARS),-D$(var)="$(strip $($(var)))")


GENVERSION = pvxsVCS.h
GENVERSIONMACRO = PVXS_VCS_VERSION


LIBRARY += pvxs
#LIBRARY += pvxsIoc
############################################################################
#
# Add any files that need to be compiled (e.g. .c, .cpp, .st, .stt)
#
############################################################################

SOURCES   += $(APPSRC)/describe.cpp
SOURCES   += $(APPSRC)/log.cpp
SOURCES   += $(APPSRC)/unittest.cpp
SOURCES   += $(APPSRC)/util.cpp
SOURCES   += $(APPSRC)/osgroups.cpp
SOURCES   += $(APPSRC)/sharedarray.cpp
SOURCES   += $(APPSRC)/bitmask.cpp
SOURCES   += $(APPSRC)/type.cpp
SOURCES   += $(APPSRC)/data.cpp
SOURCES   += $(APPSRC)/datafmt.cpp
SOURCES   += $(APPSRC)/pvrequest.cpp
SOURCES   += $(APPSRC)/dataencode.cpp
SOURCES   += $(APPSRC)/nt.cpp
SOURCES   += $(APPSRC)/evhelper.cpp
SOURCES   += $(APPSRC)/udp_collector.cpp

SOURCES   += $(APPSRC)/config.cpp
SOURCES   += $(APPSRC)/conn.cpp

SOURCES   += $(APPSRC)/server.cpp
SOURCES   += $(APPSRC)/serverconn.cpp
SOURCES   += $(APPSRC)/serverchan.cpp
SOURCES   += $(APPSRC)/serverintrospect.cpp
SOURCES   += $(APPSRC)/serverget.cpp
SOURCES   += $(APPSRC)/servermon.cpp
SOURCES   += $(APPSRC)/serversource.cpp
SOURCES   += $(APPSRC)/sharedpv.cpp

SOURCES   += $(APPSRC)/client.cpp
SOURCES   += $(APPSRC)/clientreq.cpp
SOURCES   += $(APPSRC)/clientconn.cpp
SOURCES   += $(APPSRC)/clientintrospect.cpp
SOURCES   += $(APPSRC)/clientget.cpp
SOURCES   += $(APPSRC)/clientmon.cpp

SOURCES   += $(IOCSRC)/iochooks.cpp
##pvxsIoc_SRCS += $(IOC)/iochooks.cpp
############################################################################
#
# Add any .dbd files that should be included (e.g. from user-defined functions, etc.)
#
############################################################################

#DBDS   += 
DBDS += $(IOC)/pvxsIoc.dbd
#DBDS += pvxs.dbd

############################################################################
#
# Add any header files that should be included in the install (e.g. 
# StreamDevice or asyn header files that are used by other modules)
#
############################################################################

#HEADERS   += 
HEADERS   += $(APPSRC)/bitmask.h
HEADERS   += $(APPSRC)/clientimpl.h
HEADERS   += $(APPSRC)/conn.h
HEADERS   += $(APPSRC)/dataimpl.h

HEADERS   += $(APPSRC)/evhelper.h
HEADERS   += $(APPSRC)/instcounters.h
HEADERS   += $(APPSRC)/pvaproto.h
HEADERS   += $(APPSRC)/pvrequest.h
HEADERS   += $(APPSRC)/serverconn.h
HEADERS   += $(APPSRC)/udp_collector.h
HEADERS   += $(APPSRC)/utilpvt.h

HEADERS   += $(APPINCLUDE)/client.h
HEADERS   += $(APPINCLUDE)/data.h
HEADERS   += $(APPINCLUDE)/log.h
HEADERS   += $(APPINCLUDE)/netcommon.h
HEADERS   += $(APPINCLUDE)/nt.h
HEADERS   += $(APPINCLUDE)/server.h
HEADERS   += $(APPINCLUDE)/sharedArray.h
HEADERS   += $(APPINCLUDE)/sharedpv.h
HEADERS   += $(APPINCLUDE)/source.h
HEADERS   += $(APPINCLUDE)/srvcommon.h
HEADERS   += $(APPINCLUDE)/unittest.h
HEADERS   += $(APPINCLUDE)/util.h
HEADERS   += $(APPINCLUDE)/version.h
HEADERS   += $(IOCSRC)/pvxs/iochooks.h
LIB_LIBS+=event
LIB_LIBS+=event_pthreads

############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard iocsh/*.iocsh)


############################################################################
#
# If you have any .substitution files, and template files, add them here.
#
############################################################################

# SUBS=$(wildcard $(APPDB)/*.substitutions)
# TMPS=$(wildcard $(APPDB)/*.template)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

#====================================


#----------------------------------------
#  ADD RULES AFTER THIS LINE
#
#  # Can't use EXPAND as generated headers must appear
# in O.Common, but EXPAND emits rules for O.$(T_A)
../O.Common/pvxs/versionNum.h: ../pvxs/versionNum.h@
	$(MKDIR) $(COMMON_DIR)/pvxs
	$(EXPAND_TOOL) $(EXPANDFLAGS) $($@_EXPANDFLAGS) $< $@

describe$(DEP): describe.h
util$(DEP): $(COMMON_DIR)/$(GENVERSION)

ifndef GENVERSIONHEADER
$(COMMON_DIR)/$(GENVERSION):
	echo "/* genVersionHeader.pl missing */" > $@
endif


# JT, substitution and template db files
db: $(SUBS) $(TMPS)

$(SUBS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@

$(TMPS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@

.PHONY: db $(SUBS) $(TMPS)


vlibs:

.PHONY: vlibs
